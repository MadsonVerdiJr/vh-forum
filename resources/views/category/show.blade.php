@extends('layouts.app')

@section('content')

    {{ Breadcrumbs::render('category.show', $category) }}

    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Category
                </div>
                <div class="panel-body">
                    {{ $category->name }}
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Posts
                </div>

                @include('post.components.list')
            </div>
        </div>
    </div>
@endsection
