@extends('layouts.app')

@section('content')

    {{ Breadcrumbs::render('category.index') }}

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Categories</div>

                @include('category.components.table')


            </div>
        </div>
    </div>
@endsection
