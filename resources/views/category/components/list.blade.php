<ul class="list-group">
    @forelse($categories as $category)
    <a href="{{ route('category.show', $category->slug) }}" class="list-group-item">
        @if (isset($post) and Auth::id() == $post->user_id)
            <div class="pull-right">
                @include('post.category.components.actions')
            </div>
        @endif

        {{ $category->name }}
    </a>
    @empty
    <li class="list-group-item">
        <p>No categories.</p>
    </li>
    @endforelse
    @if (isset($post) and Auth::id() == $post->user_id)
        <li class="list-group-item">
            <a href="{{ route('post.category.create', $post->slug) }}">Click here</a> to add a new one.
        </li>
    @endif
</ul>