<table class="table table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Posts</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @forelse($categories as $category)
            <tr>
                <td>{{ $category->id }}</td>
                <td>{{ $category->name }}</td>
                <td>{{ $category->posts()->count() }}</td>
                <td>@include('category.components.actions')</td>
            </tr>
        @empty
            <tr>
                <td colspan="4">No categories registered.</td>
            </tr>
        @endforelse
    </tbody>
</table>