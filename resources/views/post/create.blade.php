@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Create Posts</div>
                <div class="panel-body">

                    @include('post.components.form', ['method' => 'POST', 'route' => 'post.store'])

                </div>
            </div>
        </div>
    </div>
@endsection
