{{ Form::open(['method' => 'POST', 'route' => ['post.comment.store', $post->id]]) }}

    {{ Form::bsText('content', 'Join the discussion...') }}

    {{ Form::bsSubmit('Send comment') }}

{{ Form::close() }}