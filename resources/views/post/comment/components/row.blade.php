<ul class="list-group">


    @forelse($comments as $comment)
        <li class="list-group-item">

            <div class="row">

                <div class="col-md-3 text-muted">
                    @if ($comment->created_at != $comment->updated_at)
                        <div data-toggle="tooltip" data-placement="left" title="Updated at">
                            {{ $comment->updated_at->format('Y-m-d h:i A') }}
                        </div>
                        <small data-toggle="tooltip" data-placement="left" title="Created at">{{ $comment->created_at->format('Y-m-d h:i A') }}</small>
                    @else
                        {{ $comment->created_at->format('Y-m-d h:i A') }}
                    @endif
                </div>

                <div class="col-md-2 text-muted">{{ $comment->user->name }}</div>

                <div class="col-md-7 text-info">
                    @if ($comment->user_id == Auth::id())

                        <div class="pull-right">
                            <a href="#" class="btn btn-xs btn-default comment-delete" data-url="{{ route('post.comment.destroy', [$post->id, $comment->id]) }}" data-toggle="modal" data-target="#comment-delete-modal">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </div>

                        <a href="#" class="x-editable" data-token="{{ csrf_token() }}" data-type="text" data-url="{{ route('post.comment.update', [$post->id, $comment->id]) }}" data-title="Update comment" data-value="{{ $comment->content }}">
                            {{ $comment->content }}
                        </a>
                    @else
                        {{ $comment->content }}
                    @endif

                </div>
            </div>
        </li>
    @empty
        <li class="list-group-item">
            <div class="row">
                <div class="col-md-12">
                    No comments for this post.
                </div>
            </div>
        </li>
    @endforelse

    @if ($comments->hasPages())
        <li class="list-group-item">
            <div class="row">
                <div class="col-md-12 text-center">
                    {{ $comments->links() }}
                </div>
            </div>
        </li>
    @endif
</ul>

@push('js-helpers')
    <script>
        $(document).ready(function() {
            $('.x-editable').editable({
                send: 'always',
                ajaxOptions: {
                    type: 'PUT',
                },
                params: function (params) {
                    params._token = $(this).data("token");
                    return params;
                }
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endpush