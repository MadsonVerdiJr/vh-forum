@extends('layouts.app')

@section('content')

    {{ Breadcrumbs::render('post.show', $post) }}

    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Categories</div>
                @include('post.category.components.list', ['categories' => $categories])
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">

                        {{ $post->title }}

                    </h3>
                    <small class="text-muted">{{ $post->content_short }}</small>
                </div>
                <div class="panel-body">
                    {!! $post->content !!}
                </div>
                <div class="panel-footer">
                    @if (Auth::id() == $post->user_id)
                        <span class="pull-right">
                            @include('post.components.actions', ['post' => $post])
                        </span>
                    @endif
                    <p>
                        <small class="text-muted">Author:</small>
                        {{ $post->user->name }}
                    </p>
                    <p>
                        <small class="text-muted">Created at:</small>
                        {{ $post->created_at->format('Y-m-d h:i A') }}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Comments</div>
                <div class="panel-body">
                    {{-- Comment --}}
                    @if (Auth::guest())
                        You need to <a href="{{ route('login') }}">Login</a> to comment.
                    @else
                        @include('post.comment.components.create')
                    @endif
                </div>
                @include('post.comment.components.row')
            </div>
        </div>
    </div>
@endsection


@push('modal')
    <!-- Delete Comment Modal -->
    <div class="modal fade" id="comment-delete-modal" tabindex="-1" role="dialog">

        <div class="modal-dialog" role="document">

            <div class="modal-content">
                {{ Form::open(['method' => 'DELETE', route('post.comment.destroy', [$post->id, 0])]) }}

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Delete comment</h4>
                    </div>

                    <div class="modal-body">
                        This action cannot be undone!
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                        {{ Form::submit('Delete comment', ['class' => 'btn btn-primary']) }}
                    </div>

                {{ Form::close() }}

            </div>
        </div>
    </div>

@endpush

@push('js-helpers')
<script>
    $('#comment-delete-modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var url = button.data('url') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('form').attr('action', url);
    });
</script>
@endpush