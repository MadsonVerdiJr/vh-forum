{{ Form::open(['method' => $method, 'route' => $route]) }}

    {{ Form::bsText('title', null, isset($post) ? $post->title : null) }}

    {{ Form::bsText('content_short', 'Short description', isset($post) ? $post->content_short : null) }}

    {{ Form::bsTextarea('content', null, isset($post) ? $post->content : null) }}

    {{ Form::bsSubmit('Save') }}

{{ Form::close() }}