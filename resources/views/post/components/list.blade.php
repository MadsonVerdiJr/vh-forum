<div class="list-group">
    @if (Auth::check())
        <a href="{{ route('post.create') }}" class="list-group-item active">
            <div class="text-right">

            Create new post

            </div>
        </a>
    @endif

    @forelse($posts as $post)
        <a href="{{ route('post.show', $post->slug) }}" class="list-group-item">
            <span class="badge">{{ $post->comments()->count() }}</span>
            <h4 class="list-group-item-heading">{{ $post->title }}</h4>
            <div class="list-group-item-text">
                {{ $post->content_short }}
            </div>
        </a>
    @empty
        <div class="list-group-item">
            No posts created.
        </div>
    @endforelse
</div>

@if ($posts->hasPages())
    <div class="text-center">
        {{ $posts->appends(Request::capture()->except('page'))->links() }}
    </div>
@endif