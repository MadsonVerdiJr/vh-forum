<div class="btn-group btn-group-sm">
    <a href="{{ route('post.edit', $post->slug) }}" class="btn btn-default">Edit</a>
    <a href="#" class="btn btn-default" data-toggle="modal" data-target="#Post-{{ $post->id }}">Delete</a>
</div>

@push('modal')

    <div id="Post-{{ $post->id }}" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Post</h4>
                </div>
                <div class="modal-body">
                    <p>This action cannot be undone!</p>
                </div>
                <div class="modal-footer">
                    {{ Form::open(['method' => 'DELETE', 'route' => ['post.destroy', $post->id]]) }}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ Form::submit('Delete', ['class' => 'btn btn-primary']) }}
                    {{ Form::close() }}
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endpush