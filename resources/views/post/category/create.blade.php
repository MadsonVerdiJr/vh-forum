@extends('layouts.app')

@section('content')

    {{ Breadcrumbs::render('post.category.create', $post) }}

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Create category for "{{ $post->title }}"</div>
                <div class="panel-body">

                    @include('post.category.components.form', ['method' => 'POST', 'route' => ['post.category.store', $post->id]])

                </div>
            </div>
        </div>
    </div>
@endsection
