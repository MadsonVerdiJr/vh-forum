<ul class="list-group">
    @forelse($categories as $category)
    <li class="list-group-item">
        <a href="{{ route('category.show', $category->slug) }}">
            {{ $category->name }}
        </a>

        @if (isset($post) and Auth::id() == $post->user_id)
            <div class="pull-right">
                @include('post.category.components.actions')
            </div>
        @endif
    </li>
    @empty
    <li class="list-group-item">
        <p>This post has no categories.</p>
    </li>
    @endforelse
    @if (isset($post) and Auth::id() == $post->user_id)
        <li class="list-group-item">
            <a href="{{ route('post.category.create', $post->slug) }}">Click here</a> to add a new one.
        </li>
    @endif
</ul>