<a href="#" class="btn btn-xs btn-default" data-toggle="modal" data-target="#Post-Category-{{ $category->id }}">
    <span class="glyphicon glyphicon-trash"></span>
</a>

@push('modal')

    <div id="Post-Category-{{ $category->id }}" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Category</h4>
                </div>
                <div class="modal-body">
                    <p>This category will be deleted from this post only!</p>
                </div>
                <div class="modal-footer">
                    {{ Form::open(['method' => 'DELETE', 'route' => ['post.category.destroy', $post->id, $category->id]]) }}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        {{ Form::submit('Delete', ['class' => 'btn btn-primary']) }}
                    {{ Form::close() }}
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endpush