{{ Form::open(['method' => $method, 'route' => $route]) }}

    {{ Form::bsText('name', null, isset($category) ? $category->name : null) }}

    {{ Form::bsSubmit('Save') }}

{{ Form::close() }}