@extends('layouts.app')

@section('content')

    {{ Breadcrumbs::render('post.edit', $post) }}

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Posts</div>
                <div class="panel-body">

                    @include('post.components.form', ['method' => 'PUT', 'route' => ['post.update', $post->id]])

                </div>
            </div>
        </div>
    </div>
@endsection
