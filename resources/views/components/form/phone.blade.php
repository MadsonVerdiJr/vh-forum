
{{ Form::bsText($name, $label, $value, ['class' => 'form-control form-phone']) }}

@push('js-helpers')
    <script>
        var phoneMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        phoneOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(phoneMaskBehavior.apply({}, arguments), options);
            }
        }
        $('.form-phone').mask(phoneMaskBehavior, phoneOptions);
    </script>
@endpush
