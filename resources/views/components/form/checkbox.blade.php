<div class="form-group @if($errors->has($name)) has-error @endif">
    {{ Form::label($name, $label, ['class' => 'control-label']) }}
    {{ Form::checkbox($name, true, $value) }}
    {!! $errors->first($name, '<p class="help-block">:message</p>')  !!}
</div>
