<div class="form-group @if($errors->has($name)) has-error @endif">
    {{ Form::label($name, $label, ['class' => 'control-label']) }}
    {{ Form::textarea($name, $value, array_merge(['class' => 'form-control', 'rows' => '5'], $attributes)) }}
    {!! $errors->first($name, '<p class="help-block">:message</p>')  !!}
</div>
