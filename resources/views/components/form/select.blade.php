<div class="form-group @if($errors->has($name)) has-error @endif">
    {{ Form::label($name, $label, ['class' => 'control-label']) }}
    {{ Form::select($name, $options, $value, array_merge(['class' => 'form-control', 'style' => 'width: 100%'], $attributes)) }}
    {!! $errors->first($name, '<p class="help-block">:message</p>')  !!}
</div>

@push('js-helpers')
    <script>
        $('#{{ $name }}').select2({theme: "bootstrap"});
    </script>
@endpush
