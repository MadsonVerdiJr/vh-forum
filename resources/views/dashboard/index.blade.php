@extends('layouts.app')

@section('content')

    {{ Breadcrumbs::render('dashboard') }}

    <div class="row">

        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">My Categories</div>

                @include('category.components.list')

            </div>
        </div>

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">My Posts</div>

                @include('post.components.list')
            </div>
        </div>

        <div class="col-md-9 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">My comments</div>

                @include('comment.components.row')
            </div>
        </div>
    </div>
@endsection
