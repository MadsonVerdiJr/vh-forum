@extends('layouts.app')

@section('content')

    {{ Breadcrumbs::render('home.index') }}

    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">

                <div class="panel-heading">Filters</div>

                @include('category.components.list')
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">

                <div class="panel-heading">Posts</div>

                @include('post.components.list')


            </div>
        </div>
    </div>
@endsection
