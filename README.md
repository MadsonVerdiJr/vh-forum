# VanHack Talent Accelerator

## Stage 2: Tech Project

- Develop a simple Forum app with:
    + Landing page with all Posts;
    + Post's category;
    + Commenting system;
    + Login/Logout with permission to edit only owned Posts/Comments.


# Instalation

## Requirements

- PHP7
- Apache or Nginx
- Node JS
- Composer JS

### Environment configuration

The environment configuration can be implemented using [Docker](https://www.docker.com) or [Vagrant](https://www.vagrantup.com). All commands used are used in project root folder.

#### Docker configuration

This configuration will create some containers for running the application. The folowing commands are necessary:

1. `$ cp .env-example .env`, this will create laravel .env file
2. `$ cd ./laradock-vh-forum`
3. `$ cp env-example .env`
    + edit this file with your needed configurations
    + update data save path: `DATA_SAVE_PATH=~/.laradock-vh-forum/data`, this will prevent to override or use your existing containers
    + enable node in line: `WORKSPACE_INSTALL_NODE=true`
4. `$ docker-compose up -d nginx mysql`, this command must be executed in `./laradock-vh-forum` folder
6. `$ docker-compose exec --user=laradock workspace bash`, now the configuration process will proceed inside container.
7. `laradock $ composer install && npm install`
8. `laradock $ php artisan key:generate`
9. `laradock $ php artisan migrate`

Now application can be accessed in `http://localhost/`

#### Vagrant configuration

For vagrant instalation you must folow oficial laravel instalation: https://laravel.com/docs/5.5/homestead

# Third party implementation

- Slugs: https://github.com/cviebrock/eloquent-sluggable
- TinyMCE: https://www.tinymce.com
- Breadcrumbs: https://github.com/davejamesmiller/laravel-breadcrumbs
- Select2: https://select2.org/
- Datetimepicker: https://eonasdan.github.io/bootstrap-datetimepicker/