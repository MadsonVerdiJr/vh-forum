<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home.index');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');


Route::resource('post/{post}/category', 'PostCategoryController', ['as' => 'post'])->only(['create', 'store','destroy']);

Route::resource('post/{post}/comment', 'PostCommentController', ['as' => 'post'])->only(['store', 'update', 'destroy']);

Route::resource('post', 'PostController');

Route::resource('category', 'CategoryController');