<?php

// Home
Breadcrumbs::register('home.index', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('home.index'));
});

// Dashboard
Breadcrumbs::register('dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('Dashboard', route('dashboard'));
});

// Post
Breadcrumbs::register('post.index', function ($breadcrumbs) {
    if (Auth::guest()) {
        $breadcrumbs->parent('home.index');
    } else {
        $breadcrumbs->parent('dashboard');
    }
    $breadcrumbs->push('Posts', route('post.index'));
});

// Post show
Breadcrumbs::register('post.show', function ($breadcrumbs, $post) {
    $breadcrumbs->parent('post.index');
    $breadcrumbs->push($post->title, route('post.show', $post->slug));
});

// Post edit
Breadcrumbs::register('post.edit', function ($breadcrumbs, $post) {
    $breadcrumbs->parent('post.show', $post);
    $breadcrumbs->push('Edit', route('post.edit', $post->slug));
});

// Post Add Category
Breadcrumbs::register('post.category.create', function ($breadcrumbs, $post) {
    $breadcrumbs->parent('post.show', $post);
    $breadcrumbs->push('Create Category', route('post.category.create', $post->slug));
});

// Category
Breadcrumbs::register('category.index', function ($breadcrumbs) {
    if (Auth::guest()) {
        $breadcrumbs->parent('home.index');
    } else {
        $breadcrumbs->parent('dashboard');
    }
    $breadcrumbs->push('Categories', route('category.index'));
});

// Category show
Breadcrumbs::register('category.show', function ($breadcrumbs, $category) {
    $breadcrumbs->parent('category.index');
    $breadcrumbs->push($category->name, route('category.show', $category->name));
});