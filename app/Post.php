<?php

namespace App;

use App\User;
use App\PostCategory;
use App\PostComment;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use Sluggable, SluggableScopeHelpers;

    /**
     * Mass assignment fields
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content',
        'content_short',
        'user_id'
    ];

    /**
     * User who created Post
     *
     * @return App\User
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Categories from post
     *
     * @return App\PostCategory
     */
    public function categories()
    {
        return $this->belongsToMany(PostCategory::class);
    }

    /**
     * Comments from post
     *
     * @return App\PostComment
     */
    public function comments()
    {
        return $this->hasMany(PostComment::class);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer                               $category - Category ID
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCategoryId($query, $category)
    {
        return $query->whereHas('categories', function ($categories) use ($category) {
            $categories->whereId($category);
        });
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer                               $category - Category ID
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCategorySlug($query, $category)
    {
        return $query->whereHas('categories', function ($categories) use ($category) {
            $categories->whereSlug($category);
        });
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
