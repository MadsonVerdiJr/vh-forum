<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Post repository
        $this->app->bind(
            'App\Repositories\PostRepositoryInterface',
            'App\Repositories\PostRepository'
        );

        // Post Category repository
        $this->app->bind(
            'App\Repositories\PostCategoryRepositoryInterface',
            'App\Repositories\PostCategoryRepository'
        );

        // Post Comments repository
        $this->app->bind(
            'App\Repositories\PostCommentRepositoryInterface',
            'App\Repositories\PostCommentRepository'
        );
    }
}
