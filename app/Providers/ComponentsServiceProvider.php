<?php

namespace App\Providers;

use Collective\Html\FormFacade as Form;
use Illuminate\Support\ServiceProvider;

class ComponentsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //////////////
        // Checkbox //
        //////////////
        Form::component(
            'bsCheckbox',
            'components.form.checkbox',
            [
                'name',
                'label' => null,
                'value' => null,
                'attributes' => []
            ]
        );

        //////////////
        // Datetime //
        //////////////
        Form::component(
            'bsDatetime',
            'components.form.datetime',
            [
                'name',
                'label' => null,
                'value' => null,
                'attributes' => [],
            ]
        );

        //////////////////
        // Dynamic Text //
        //////////////////
        Form::component(
            'bsDynamicText',
            'components.form.dynamic_text',
            [
                'name',
                'label' => null,
                'values' => [],
                'attributes' => []
            ]
        );

        ////////////
        // E-mail //
        ////////////
        Form::component(
            'bsEmail',
            'components.form.email',
            [
                'name',
                'label' => null,
                'value' => null,
                'attributes' => []
            ]
        );

        //////////////
        // Password //
        //////////////
        Form::component(
            'bsPassword',
            'components.form.password',
            [
                'name',
                'label' => null,
                'value' => null,
                'attributes' => []
            ]
        );

        ////////////
        // Phone //
        ////////////
        Form::component(
            'bsPhone',
            'components.form.phone',
            [
                'name',
                'label' => null,
                'value' => null,
                'attributes' => []
            ]
        );

        ////////////
        // Phones //
        ////////////
        Form::component(
            'bsPhones',
            'components.form.phones',
            [
                'name',
                'label' => null,
                'values' => [],
                'attributes' => []
            ]
        );

        ////////////
        // Select //
        ////////////
        Form::component(
            'bsSelect',
            'components.form.select',
            [
                'name',
                'label' => null,
                'options' => [],
                'value' => null,
                'attributes' => []
            ]
        );

        ////////////
        // Submit //
        ////////////
        Form::component(
            'bsSubmit',
            'components.form.submit',
            [
                'value',
                'attributes' => []
            ]
        );

        //////////
        // Text //
        //////////
        Form::component(
            'bsText',
            'components.form.text',
            [
                'name',
                'label' => null,
                'value' => null,
                'attributes' => []
            ]
        );

        //////////////
        // Textarea //
        //////////////
        Form::component(
            'bsTextarea',
            'components.form.textarea',
            [
                'name',
                'label' => null,
                'value' => null,
                'attributes' => []
            ]
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
