<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use SebastianBergmann\Version;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $version = new Version('v0.1.2', __DIR__);

        view()->share('version', $version->getVersion());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
