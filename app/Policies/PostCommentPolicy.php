<?php

namespace App\Policies;

use App\User;
use App\PostComment;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostCommentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the postComment.
     *
     * @param  \App\User  $user
     * @param  \App\PostComment  $postComment
     *
     * @return mixed
     */
    public function view(User $user, PostComment $postComment)
    {
        //
    }

    /**
     * Determine whether the user can create postComments.
     *
     * @param  \App\User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the postComment.
     *
     * @param  \App\User  $user
     * @param  \App\PostComment  $postComment
     *
     * @return mixed
     */
    public function update(User $user, PostComment $postComment)
    {
        return $user->id === $postComment->user_id;
    }

    /**
     * Determine whether the user can delete the postComment.
     *
     * @param  \App\User  $user
     * @param  \App\PostComment  $postComment
     *
     * @return mixed
     */
    public function delete(User $user, PostComment $postComment)
    {
        return $user->id === $postComment->user_id;
    }
}
