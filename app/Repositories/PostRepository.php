<?php

namespace App\Repositories;

use App\Post;
use App\Repositories\PostRepositoryInterface;

class PostRepository implements PostRepositoryInterface
{

    /**
     * Post model
     *
     * @var App\Post
     */
    private $post_model;

    /**
     * Apply filters in current model
     *
     * @param  array  $filters - Array of filters
     *
     * @return App\Post
     */
    private function applyFilters($filters = [])
    {
        if (isset($filters['user_id'])) {
            $this->post_model = $this->post_model->whereUserId($filters['user_id']);
        }

        if (isset($filters['category_id'])) {
            $this->post_model = $this->post_model->categoryId($filters['category_id']);
        }

        if (isset($filters['category_slug'])) {
            $this->post_model = $this->post_model->categorySlug($filters['category_slug']);
        }


        return $this->post_model;
    }

    /**
     * Class constructor
     *
     * @param Post $post - Post model
     */
    public function __construct(Post $post)
    {
        $this->post_model = $post;
    }

    /**
     * Add one or more categories
     *
     * @param integer       $id         - Post ID
     * @param integer|array $category   - Category ID or array or ID
     */
    public function addCategory($id, $category)
    {
        $this->post_model = $this->post_model->findOrFail($id);
        $this->post_model->categories()->attach($category);
    }

    /**
     * Create a post
     *
     * @param  string  $title           - Title of the post
     * @param  string  $content         - Description of the post
     * @param  string  $content_short   - Short Description of the post
     * @param  integer $author_id       - Author of the post
     *
     * @return App\Post
     */
    public function create($title, $content, $content_short, $author_id)
    {
        $this->post_model = $this->post_model->create([
            'title' => $title,
            'content' => $content,
            'content_short' => $content_short,
            'user_id' => $author_id,
        ]);

        return $this->post_model;
    }

    /**
     * Delete a post
     *
     * @param  integer $id - Post ID
     *
     * @return boolean
     */
    public function delete($id)
    {
        $this->post_model->destroy($id);

        return true;
    }

    /**
     * Get all posts paginated
     *
     * @param  array   $filters - Filters used for pagination
     * @param  integer $perPage - The number of items to be shown per page
     *
     * @return Illuminate\Pagination\Paginator
     */
    public function getAllPaginated($filters = [], $perPage = 15)
    {
        $this->post_model = $this->applyFilters($filters);

        return $this->post_model->paginate($perPage);
    }

    /**
     * Get post author ID
     *
     * @param  integer $id - Post ID
     *
     * @return integer
     */
    public function getAuthorId($id)
    {
        return $this->post_model->findOrFail($id)->user->id;
    }

    /**
     * Get a post with an ID
     *
     * @param  integer $id - Post ID
     *
     * @return App\Post
     */
    public function getById($id)
    {
        return $this->post_model->findOrFail($id);
    }


    /**
     * Get a post by slug
     *
     * @param  string $slug
     *
     * @return App\Post
     */
    public function getBySlug($slug)
    {
        return $this->post_model->findBySlugOrFail($slug);
    }

    /**
     * Remove one or more categories from the post,
     * categories remain on the system.
     *
     * @param  integer          $id       - Post ID
     * @param  integer|array    $category - Category ID or array of IDs
     */
    public function removeCategory($id, $category)
    {
        $this->post_model = $this->post_model->findOrFail($id);
        $this->post_model->categories()->detach($category);
    }

    /**
     * Update post
     *
     * @param  integer  $id             - Post ID
     * @param  string   $title          - Title of the post
     * @param  string   $content        - Description of the post
     * @param  string   $content_short  - Short Description of the post
     * @param  integer  $author_id      - Author of the post
     *
     * @return App\Post
     */
    public function update($id, $title, $content, $content_short, $author_id = null)
    {
        $update = [
            'title' => $title,
            'content' => $content,
            'content_short' => $content_short,
        ];

        if ($author_id) {
            $update['user_id'] = $author_id;
        }

        return $this->post_model->findOrFail($id)->update($update);
    }
}
