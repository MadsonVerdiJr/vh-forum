<?php

namespace App\Repositories;

use App\PostComment;

class PostCommentRepository implements PostCommentRepositoryInterface
{
    /**
     * Post Comment model
     *
     * @var App\PostComment
     */
    private $post_comment_model;

    /**
     * Apply filters in current model
     *
     * @param  array  $filters
     *
     * @return array
     */
    private function applyFilters($filters = [])
    {
        if (isset($filters['user_id'])) {
            $this->post_comment_model = $this->post_comment_model->whereUserId($filters['user_id']);
        }

        if (isset($filters['post_id'])) {
            $this->post_comment_model = $this->post_comment_model->postId($filters['post_id']);
        }

        if (isset($filters['post_slug'])) {
            $this->post_comment_model = $this->post_comment_model->postSlug($filters['post_slug']);
        }

        if (isset($filters['order_by'])) {
            $this->post_comment_model = $this->post_comment_model->orderBy($filters['order_by'], 'DESC');
        } else {
            $this->post_comment_model = $this->post_comment_model->orderBy('updated_at', 'DESC');
        }

        return $this->post_comment_model;
    }

    /**
     * Class constructor
     *
     * @param PostComment $post_comment_model - Post Comment model
     */
    public function __construct(PostComment $post_comment_model)
    {
        $this->post_comment_model = $post_comment_model;
    }

    /**
     * Create a comment for a post by a user
     *
     * @param  string  $content - Content of the comment
     * @param  integer $post_id - Post ID for the comment
     * @param  integer $user_id - User ID from user who created the post
     *
     * @return App\PostComment
     */
    public function create($content, $post_id, $user_id)
    {
        $this->post_comment_model = $this->post_comment_model->create([
            'content' => $content,
            'post_id' => $post_id,
            'user_id' => $user_id,
        ]);

        return $this->post_comment_model;
    }

    /**
     * Delete a comment
     *
     * @param  integer $id - Comment ID
     *
     * @return void
     */
    public function delete($id)
    {
        $this->post_comment_model->destroy($id);
    }

    /**
     * Delete all comments from post
     *
     * @param  integer $post_id - Post ID
     *
     * @return void
     */
    public function deleteAllFromPost($post_id)
    {
        $this->post_comment_model->wherePostId($post_id)->delete();
    }

    /**
     * Get all comments paginated
     *
     * @param  array   $filters - Filters used for pagination
     * @param  integer $perPage - The number of items to be shown per page
     *
     * @return Illuminate\Pagination\Paginator
     */
    public function getAllPaginated($filters = [], $perPage = 15)
    {
        $this->post_comment_model = $this->applyFilters($filters);

        return $this->post_comment_model->paginate($perPage);
    }

    /**
     * Get comment by ID
     *
     * @param  integer $id - Comment ID
     *
     * @return App\PostComment
     */
    public function getById($id)
    {
        return $this->post_comment_model->findOrFail($id);
    }

    /**
     * Update a comment
     *
     * @param  integer $id      - Comment ID
     * @param  string  $content - Comment new content
     *
     * @return App\PostComment
     */
    public function update($id, $content)
    {
        $this->post_comment_model = $this->post_comment_model->findOrFail($id);

        $this->post_comment_model->update(['content' => $content]);

        return $this->post_comment_model;
    }
}
