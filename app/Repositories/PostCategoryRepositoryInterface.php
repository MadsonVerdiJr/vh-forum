<?php

namespace App\Repositories;

interface PostCategoryRepositoryInterface
{
    /**
     * Create a category
     *
     * @param  string  $name    - Category name
     * @param  integer $user_id - User ID
     *
     * @return App\PostCategory
     */
    public function create($name, $user_id);

    /**
     * Geta all categories
     *
     * @param  array  $filters - Filters used for pagination
     *
     * @return App\PostCategory
     */
    public function getAll($filters = []);

    /**
     * Get all categories paginated
     *
     * @param  array  $filters - Filters used for pagination
     *
     * @return Illuminate\Pagination\Paginator
     */
    public function getAllPaginated($filters = [], $perPage = 15);

    /**
     * Get category by ID
     *
     * @param  integer $id - Category ID
     *
     * @return App\PostCategory
     */
    public function getById($id);

    /**
     * Get a category by slug
     *
     * @param  string $slug
     *
     * @return App\Post
     */
    public function getBySlug($slug);

    /**
     * Get category by ID and Post ID
     *
     * @param  integer $id      - Category ID
     * @param  integer $post_id - Post ID
     *
     * @return App\PostCategory
     */
    public function getByIdAndPostId($id, $post_id);
}
