<?php

namespace App\Repositories;

interface PostCommentRepositoryInterface
{
    /**
     * Create a comment for a post by a user
     *
     * @param  string  $content - Content of the comment
     * @param  integer $post_id - Post ID for the comment
     * @param  integer $user_id - User ID from user who created the post
     *
     * @return App\PostComment
     */
    public function create($content, $post_id, $user_id);

    /**
     * Delete a comment
     *
     * @param  integer $id - Comment ID
     *
     * @return void
     */
    public function delete($id);

    /**
     * Delete all comments from post
     *
     * @param  integer $post_id - Post ID
     *
     * @return void
     */
    public function deleteAllFromPost($post_id);

    /**
     * Get all comments paginated
     *
     * @param  array   $filters - Filters used for pagination
     * @param  integer $perPage - The number of items to be shown per page
     *
     * @return Illuminate\Pagination\Paginator
     */
    public function getAllPaginated($filters = [], $perPage = 15);

    /**
     * Get comment by ID
     *
     * @param  integer $id - Comment ID
     *
     * @return App\PostComment
     */
    public function getById($id);

    /**
     * Update a comment
     *
     * @param  integer $id      - Comment ID
     * @param  string  $content - Comment new content
     *
     * @return App\PostComment
     */
    public function update($id, $content);
}
