<?php

namespace App\Repositories;

use App\PostCategory;

class PostCategoryRepository implements PostCategoryRepositoryInterface
{
    private $post_category_model;

    private function applyFilters($filters = [])
    {
        \Log::info($filters);
        if (isset($filters['user_id'])) {
            $this->post_category_model = $this->post_category_model->whereUserId($filters['user_id']);
        }

        if (isset($filters['post_id'])) {
            $this->post_category_model = $this->post_category_model->postId($filters['post_id']);
        }

        if (isset($filters['post_slug'])) {
            $this->post_category_model = $this->post_category_model->postSlug($filters['post_slug']);
        }

        if (isset($filters['order_by'])) {
            $this->post_category_model = $this->post_category_model->orderBy($filters['order_by']);
        } else {
            $this->post_category_model = $this->post_category_model->orderBy('name');
        }

        return $this->post_category_model;
    }

    public function __construct(PostCategory $post_category_model)
    {
        $this->post_category_model = $post_category_model;
    }

    /**
     * Create a category
     *
     * @param  string  $name    - Category name
     * @param  integer $user_id - User ID
     *
     * @return App\PostCategory
     */
    public function create($name, $user_id)
    {
        $this->post_category_model = $this->post_category_model->firstOrNew(['name' => $name], ['user_id' => $user_id]);

        $this->post_category_model->save();

        return $this->post_category_model;
    }

    /**
     * Geta all categories
     *
     * @param  array  $filters - Filters used for pagination
     *
     * @return App\PostCategory
     */
    public function getAll($filters = [])
    {
        $this->post_category_model = $this->applyFilters($filters);

        return $this->post_category_model->get();
    }

    /**
     * Get all posts paginated
     *
     * @param  array  $filters - Filters used for pagination
     *
     * @return Illuminate\Pagination\Paginator
     */
    public function getAllPaginated($filters = [], $perPage = 15)
    {
        $this->post_category_model = $this->applyFilters($filters);

        return $this->post_category_model->paginate($perPage);
    }

    /**
     * Get category by ID
     *
     * @param  integer $id - Category ID
     *
     * @return App\PostCategory
     */
    public function getById($id)
    {
        return $this->post_category_model->findOrFail($id);
    }

    /**
     * Get a category by slug
     *
     * @param  string $slug
     *
     * @return App\Post
     */
    public function getBySlug($slug)
    {
        return $this->post_category_model->findBySlugOrFail($slug);
    }

    /**
     * Get category by ID and Post ID
     *
     * @param  integer $id      - Category ID
     * @param  integer $post_id - Post ID
     *
     * @return App\PostCategory
     */
    public function getByIdAndPostId($id, $post_id)
    {
        return $this->post_category_model->postId($post_id)->whereId($id)->first();
    }
}
