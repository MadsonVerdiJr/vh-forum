<?php

namespace App\Repositories;

interface PostRepositoryInterface
{

    /**
     * Add one or more categories
     *
     * @param integer       $id         - Post ID
     * @param integer|array $category   - Category ID or array or ID
     */
    public function addCategory($id, $category);

    /**
     * Create a post
     *
     * @param  string  $title           - Title of the post
     * @param  string  $content         - Description of the post
     * @param  string  $content_short   - Short Description of the post
     * @param  integer $author_id       - Author of the post
     *
     * @return App\Post
     */
    public function create($title, $content, $content_short, $author_id);

    /**
     * Delete a post
     *
     * @param  integer $id - Post ID
     *
     * @return boolean
     */
    public function delete($id);

    /**
     * Get all posts paginated
     *
     * @param  array  $filters - Filters used for pagination
     *
     * @return Illuminate\Pagination\Paginator
     */
    public function getAllPaginated($filters = [], $perPage = 15);

    /**
     * Get post author ID
     *
     * @param  integer $id - Post ID
     *
     * @return integer
     */
    public function getAuthorId($id);

    /**
     * Get a post with an ID
     *
     * @param  integer $id - Post ID
     *
     * @return App\Post
     */
    public function getById($id);

    /**
     * Get a post by slug
     *
     * @param  string $slug
     *
     * @return App\Post
     */
    public function getBySlug($slug);

    /**
     * Remove one or more categories from the post,
     * categories remain on the system.
     *
     * @param  integer          $id       - Post ID
     * @param  integer|array    $category - Category ID or array of IDs
     */
    public function removeCategory($id, $category);

    /**
     * Update post
     *
     * @param  integer  $id             - Post ID
     * @param  string   $title          - Title of the post
     * @param  string   $content        - Description of the post
     * @param  string   $content_short  - Short Description of the post
     * @param  integer  $author_id      - Author of the post
     *
     * @return App\Post
     */
    public function update($id, $title, $content, $content_short, $author_id = null);
}
