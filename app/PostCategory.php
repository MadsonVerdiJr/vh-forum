<?php

namespace App;

use App\Post;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model
{
    use Sluggable, SluggableScopeHelpers;

    protected $fillable = [
        'name',
        'user_id',
    ];

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer                               $post_id - Post ID
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePostId($query, $post_id)
    {
        return $query->whereHas('posts', function ($posts) use ($post_id) {
            $posts->whereId($post_id);
        });
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $slug   - Post Slug
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePostSlug($query, $slug)
    {
        return $query->whereHas('posts', function ($posts) use ($slug) {
            $posts->whereSlug($slug);
        });
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
