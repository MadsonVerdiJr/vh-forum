<?php

namespace App;

use App\Post;
use App\User;
use Illuminate\Database\Eloquent\Model;

class PostComment extends Model
{
    protected $fillable = [
        'content',
        'user_id',
        'post_id',
    ];

    /**
     * Post from comment
     *
     * @return App\Post
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    /**
     * User who created comment
     *
     * @return App\User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Scope a query to only include with current ID.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer                               $post_id - Post ID
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePostId($query, $post_id)
    {
        return $query->wherePostId($post_id);
    }

    /**
     * Scope a query to only include with current slug.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $post_id - Post ID
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePostSlug($query, $slug)
    {
        return $query->whereHas('post', function ($post) use ($slug) {
            $post->whereSlug($slug);
        });
    }
}
