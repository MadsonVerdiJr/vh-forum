<?php

namespace App\Http\Controllers;

use App\Repositories\PostCategoryRepositoryInterface;
use App\Repositories\PostCommentRepositoryInterface;
use App\Repositories\PostRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Filters used for pagination
     *
     * @var array
     */
    private $filters;

    /**
     * Post Category repository
     *
     * @var App\Repositories\PostCategoryRepositoryInterface
     */
    private $post_category_repository;

    /**
     * Post Comment repository
     *
     * @var App\Repositories\PostCategoryRepositoryInterface
     */
    private $post_comment_repository;

    /**
     * Post repository
     *
     * @var App\Repositories\PostCategoryRepositoryInterface
     */
    private $post_repository;

    /**
     * Class constructor
     *
     * @param PostCategoryRepositoryInterface $post_category_repository - Post Category repository
     * @param PostCommentRepositoryInterface  $post_comment_repository  - Post Comment repository
     * @param PostRepositoryInterface         $post_repository          - Post repository
     */
    public function __construct(
        PostCategoryRepositoryInterface $post_category_repository,
        PostCommentRepositoryInterface $post_comment_repository,
        PostRepositoryInterface $post_repository
    ) {
        $this->middleware('auth');

        $this->post_category_repository = $post_category_repository;
        $this->post_comment_repository = $post_comment_repository;
        $this->post_repository = $post_repository;

    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->filters = $this->getFilters($request);
        $this->filters['user_id'] = $request->user()->id;

        $posts = $this->post_repository->getAllPaginated($this->filters);

        $comments = $this->post_comment_repository->getAllPaginated($this->filters);

        $categories = $this->post_category_repository->getAllPaginated($this->filters);

        return view('dashboard.index', compact('posts', 'comments', 'categories'));
    }
}
