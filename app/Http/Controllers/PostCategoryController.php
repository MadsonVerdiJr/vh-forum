<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryCreateRequest;
use App\Http\Requests\PostCategoryDeleteRequest;
use App\Repositories\PostCategoryRepositoryInterface;
use App\Repositories\PostRepositoryInterface;
use Illuminate\Http\Request;

class PostCategoryController extends Controller
{
    /**
     * Post repository
     *
     * @var App\Repositories\PostRepositoryInterface
     */
    private $post_repository;

    /**
     * Post Category repository
     *
     * @var App\Repositories\PostCategoryRepositoryInterface
     */
    private $post_category_repository;

    /**
     * Class constructor
     *
     * @param PostCategoryRepositoryInterface $post_category_repository - Post Category repository
     * @param PostRepositoryInterface         $post_repository          - Post repository
     */
    public function __construct(
        PostCategoryRepositoryInterface $post_category_repository,
        PostRepositoryInterface $post_repository
    ) {
        $this->middleware('auth');

        $this->post_category_repository = $post_category_repository;
        $this->post_repository = $post_repository;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $post = is_numeric($request->post) ? $this->post_repository->getById($request->post) : $this->post_repository->getBySlug($request->post);

        return view('post.category.create', compact('post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryCreateRequest $request)
    {
        $post = is_numeric($request->post) ? $this->post_repository->getById($request->post) : $this->post_repository->getBySlug($request->post);

        $category = $this->post_category_repository->create($request->name, $request->user()->id);

        $this->post_repository->addCategory($request->post, $category->id);

        if ($request->wantsJson()) {
            return response()
                ->json($category, 201)
                ->header('Location', route('category.show', [$request->post, $category->id]));
        } else {
            return redirect()
                ->route(isset($request->post) ? 'post.show' : 'category.show', isset($request->post) ? $post->slug : $request->category);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(PostCategoryDeleteRequest $request, $id)
    {
        $post = is_numeric($request->post) ? $this->post_repository->getById($request->post) : $this->post_repository->getBySlug($request->post);

        $this->post_repository->removeCategory($request->post, $request->category);

        if ($request->wantsJson()) {
            return response($id, 200);
        } else {
            return redirect()->route('post.show', [$post->slug]);
        }
    }
}
