<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostCommentCreateRequest;
use App\Http\Requests\PostCommentDeleteRequest;
use App\Http\Requests\PostCommentUpdateRequest;
use App\Repositories\PostCommentRepositoryInterface;
use App\Repositories\PostRepositoryInterface;
use Illuminate\Http\Request;

class PostCommentController extends Controller
{
    /**
     * Post Comment repository
     *
     * @var App\Repositories\PostCommentRepositoryInterface
     */
    private $post_comment_repository;

    /**
     * Post repository
     *
     * @var App\Repositories\PostRepositoryInterface
     */
    private $post_repository;

    /**
     * Class contructor
     *
     * @param PostCommentRepositoryInterface $post_comment_repository - Post Comment repository
     */
    public function __construct(
        PostCommentRepositoryInterface $post_comment_repository,
        PostRepositoryInterface $post_repository
    ) {
        $this->middleware('auth');

        $this->post_comment_repository = $post_comment_repository;
        $this->post_repository = $post_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(PostCommentCreateRequest $request)
    {
        $post = is_numeric($request->post) ? $this->post_repository->getById($request->post) : $this->post_repository->getBySlug($request->post);

        $comment = $this->post_comment_repository->create(
            $request->content,
            $request->post,
            $request->user()->id
        );

        if ($request->wantsJson()) {
            return response()
                ->json($comment, 201)
                ->header('Location', route('post.show', $post->slug));
        } else {
            return redirect()
                ->route('post.show', $post->slug);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(PostCommentUpdateRequest $request, $id)
    {
        $comment = $this->post_comment_repository->update($request->comment, $request->value);

        return response()
            ->json($comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(PostCommentDeleteRequest $request, $id)
    {
        $comment = $this->post_comment_repository->delete($request->comment);

        return redirect()->route('post.show', $request->post);
    }
}
