<?php

namespace App\Http\Controllers;

use App\Repositories\PostCategoryRepositoryInterface;
use App\Repositories\PostRepositoryInterface;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Filters used for pagination
     *
     * @var array
     */
    private $filters;

    /**
     * Post Category repository
     *
     * @var App\Repositories\PostCategoryRepositoryInterface
     */
    private $post_category_repository;

    /**
     * Post repository
     *
     * @var App\Repositories\PostCategoryRepositoryInterface
     */
    private $post_repository;

    /**
     * Class constructor
     *
     * @param PostCategoryRepositoryInterface $post_category_repository - Post Category repository
     * @param PostCommentRepositoryInterface  $post_comment_repository  - Post Comment repository
     * @param PostRepositoryInterface         $post_repository          - Post repository
     */
    public function __construct(
        PostCategoryRepositoryInterface $post_category_repository,
        PostRepositoryInterface $post_repository
    ) {
        $this->middleware('guest');

        $this->post_category_repository = $post_category_repository;
        $this->post_repository = $post_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->filters = $this->getFilters($request);

        $posts = $this->post_repository->getAllPaginated($this->filters);

        $categories = $this->post_category_repository->getAllPaginated($this->filters);

        return view('home.index', compact('posts', 'categories'));
    }
}
