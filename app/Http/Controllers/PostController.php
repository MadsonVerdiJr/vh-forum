<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostCreateRequest;
use App\Http\Requests\PostDeleteRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Repositories\PostCategoryRepositoryInterface;
use App\Repositories\PostCommentRepositoryInterface;
use App\Repositories\PostRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Post Category Repository
     *
     * @var App\Repositories\PostCategoryRepositoryInterface
     */
    private $post_category_repository;

    /**
     * Post Comment repository
     *
     * @var App\Repositories\PostCommentRepositoryInterface
     */
    private $post_comment_repository;

    /**
     * Post repository
     *
     * @var App\Repositories\PostRepositoryInterface
     */
    private $post_repository;

    /**
     * Class constructor
     *
     * @param PostCategoryRepositoryInterface   $post_category_repository   - Post Category repository
     * @param PostRepositoryInterface           $post_repository            - Post Repository
     */
    public function __construct(
        PostCategoryRepositoryInterface $post_category_repository,
        PostCommentRepositoryInterface $post_comment_repository,
        PostRepositoryInterface $post_repository
    ) {
        $this->middleware('auth')->except(['index', 'show']);

        $this->post_category_repository = $post_category_repository;
        $this->post_comment_repository = $post_comment_repository;
        $this->post_repository = $post_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = $this->getFilters($request);

        $posts = $this->post_repository->getAllPaginated($filters);

        $categories = $this->post_category_repository->getAll($filters);

        return view('post.index', compact('posts', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\PostCreateRequest  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(PostCreateRequest $request)
    {
        $post = $this->post_repository->create(
            $request->title,
            $request->content,
            $request->content_short,
            Auth::id()
        );

        if ($request->wantsJson()) {
            return response()
                ->json($post, 201)
                ->header('Location', route('post.show', $post->slug));
        } else {
            return redirect()
                ->route('post.show', $post->slug);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $filters = $this->getFilters($request);

        $post = is_numeric($request->post) ? $this->post_repository->getById($request->post) : $this->post_repository->getBySlug($request->post);

        $categories = $this->post_category_repository->getAllPaginated($filters);

        $comments = $this->post_comment_repository->getAllPaginated($filters);

        return view('post.show', compact('post', 'categories', 'comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $post = is_numeric($request->post) ? $this->post_repository->getById($request->post) : $this->post_repository->getBySlug($request->post);

        $categories = $this->post_category_repository->getAllPaginated($this->getFilters($request));

        return view('post.edit', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(PostUpdateRequest $request, $id)
    {
        $post = $this->post_repository->update(
            $id,
            $request->title,
            $request->content,
            $request->content_short
        );

        $post = $this->post_repository->getById($request->post);

        return response()
            ->json($post)
            ->header('Location', route('post.show', $post->slug));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  PostDeleteRequest $request - Request for post delete
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(PostDeleteRequest $request, $id)
    {
        $post = $this->post_repository->getById($request->post);

        // Delete commnets
        $this->post_comment_repository->deleteAllFromPost($request->post);

        // Remove categories
        $this->post_repository->removeCategory($request->post, $post->categories()->pluck('id')->toArray());

        // Delete post
        $this->post_repository->delete($request->post);

        if ($request->wantsJson()) {
            return response($id, 200);
        } else {
            return redirect()
                ->route('post.index');
        }
    }
}
