<?php

namespace App\Http\Controllers;

use App\Repositories\PostCategoryRepositoryInterface;
use App\Repositories\PostRepositoryInterface;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Category repository
     *
     * @var PostCategoryRepositoryInterface
     */
    private $category_repository;

    /**
     * Post repository
     *
     * @var App\Repositories\PostRepositoryInterface
     */
    private $post_repository;

    /**
     * Class constructor
     *
     * @param PostCategoryRepositoryInterface   $category_repository    - Category repository
     * @param PostRepositoryInterface           $post_repository        - Post repository
     */
    public function __construct(
        PostCategoryRepositoryInterface $category_repository,
        PostRepositoryInterface $post_repository
    ) {
        $this->middleware('auth')->except(['index', 'show']);

        $this->category_repository = $category_repository;
        $this->post_repository = $post_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = $this->getFilters($request);

        $categories = $this->category_repository->getAll($filters);

        return view('category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $filters = $this->getFilters($request);

        $category = is_numeric($request->category) ? $this->category_repository->getById($request->category) : $this->category_repository->getBySlug($request->category);

        $posts = $this->post_repository->getAllPaginated($filters);

        return view('category.show', compact('category', 'posts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
