<?php

namespace App\Http\Requests;

use App\Repositories\PostCategoryRepositoryInterface;
use Illuminate\Foundation\Http\FormRequest;

class PostCategoryDeleteRequest extends FormRequest
{
    /**
     * Post repository
     *
     * @var App\Repositories\PostCategoryRepositoryInterface
     */
    private $post_category_repository;

    public function __construct(PostCategoryRepositoryInterface $post_category_repository)
    {
        $this->post_category_repository = $post_category_repository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $category = $this->post_category_repository->getByIdAndPostId($this->route('category'), $this->route('post'));

        return $category !== null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
