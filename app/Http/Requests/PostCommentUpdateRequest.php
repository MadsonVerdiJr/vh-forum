<?php

namespace App\Http\Requests;

use App\Repositories\PostCommentRepositoryInterface;
use Illuminate\Foundation\Http\FormRequest;

class PostCommentUpdateRequest extends FormRequest
{
    /**
     * Post Comment repository
     *
     * @var App\Repositories\PostCommentRepositoryInterface
     */
    private $post_comment_repository;

    /**
     * Class constructor
     *
     * @param PostCommentRepositoryInterface $post_comment_repository - Post Comment repository
     */
    public function __construct(PostCommentRepositoryInterface $post_comment_repository)
    {
        $this->post_comment_repository = $post_comment_repository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $comment = $this->post_comment_repository->getById($this->comment);

        return $comment and $this->user()->can('update', $comment);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'value' => 'required|min:3|max:300'
        ];
    }
}
