<?php

namespace App\Http\Requests;

use App\Repositories\PostRepositoryInterface;
use Illuminate\Foundation\Http\FormRequest;

class PostUpdateRequest extends FormRequest
{
    /**
     * Post repository
     *
     * @var App\Repositories\PostRepositoryInterface
     */
    private $post_repository;

    public function __construct(PostRepositoryInterface $post_repository)
    {
        $this->post_repository = $post_repository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $post = $this->post_repository->getById($this->route('post'));

        return $post && $this->user()->can('update', $post);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|max:255',
            'content_short' => 'required|min:3|max:255',
            'content' => 'required|min:10',
        ];
    }
}
